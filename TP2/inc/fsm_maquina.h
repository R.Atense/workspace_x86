#ifndef _MAQUINA_CAFE_H_
#define _MAQUINA_CAFE_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    ESPERANDO_FICHA,
    SELECCION,
    EROGACION_DE_TE,
    EROGACION_DE_CAFE,
    ALARMA
} FSM_MAQUINA_CAFE_STATES_T;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Inicializacion y evaluacion de la FSM
void fsm_maquina_init(void);
void fsm_maquina_runCycle(void);

// Eventos
void fsm_maquina_cafe_evTick1Hz(void);
void fsm_maquina_cafe_evTick5Hz(void);
void fsm_maquina_cafe_evSeleccion1(void);
void fsm_maquina_cafe_evSeleccion2(void);
void fsm_maquina_cafe_evDetector(void);

// Debugging
void fsm_maquina_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _MAQUINA_CAFE_H_ */
