#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT  27  // ASCII para la tecla Esc
#define FICHA  48    // ASCII para la tecla 0  
#define SELECCION_1  49  // ASCII para la tecla 1
#define SELECCION_2  50  // ASCII para la tecla 2

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_Led1_parpadeando(void);
void hw_Led1_encendido(void);
void hw_Led2_parpadeando(void);
void hw_Apagar_led2(void);
void hw_Encender_alarma(void);
void hw_ApagarAlarma(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
