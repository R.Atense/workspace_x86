/*==================[inclusions]=============================================*/

#include "fsm_maquina.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_2_SEG  2
#define ESPERA_30_SEG  30
#define EROGACION_TE  150
#define EROGACION_CAFE  225

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

static FSM_MAQUINA_CAFE_STATES_T state;

static bool evTick1Hz;
static bool evTick5Hz;
static bool evDetector;
static bool evSeleccion1;
static bool evSeleccion2;
static uint8_t contador;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    evTick1Hz = 0;
    evTick5Hz = 0;
    evDetector = 0;
    evSeleccion1 = 0;
    evSeleccion2 = 0;
}

/*==================[external functions definition]==========================*/

void fsm_maquina_init(void)
{
    state = ESPERANDO_FICHA;
    clearEvents();
}

void fsm_maquina_runCycle(void)
{
    // El diagrama se encuentra en fsm_garaje/media/fsm_garaje_diagrama.png
    switch (state) {
        case ESPERANDO_FICHA:
            if (evTick1Hz) {
                hw_Led1_parpadeando();
            }
            else if (evDetector) {
                hw_Led1_encendido();
                contador = 0;
                state = SELECCION;
            }
            break;

        case SELECCION:
            if (evSeleccion1) {
                contador = 0;
                state = EROGACION_DE_TE;
            }
            else if (evSeleccion2) {
                contador = 0;
                state = EROGACION_DE_CAFE;
            }
            else if (evTick1Hz && (contador < ESPERA_30_SEG)) {
                contador++;
            }
            else if (evTick1Hz && (contador == ESPERA_30_SEG)) {
                state = ESPERANDO_FICHA;
            }
            break;

        case EROGACION_DE_TE:
            if (evTick5Hz && (contador < EROGACION_TE)) {
                hw_Led2_parpadeando();
                contador++;
            }
            else if (evTick5Hz && (contador == EROGACION_TE)){
                contador = 0;
                hw_Apagar_led2();
                hw_Encender_alarma();
                state = ALARMA;
            }
            break;

       case EROGACION_DE_CAFE:
            if (evTick5Hz && (contador < EROGACION_CAFE)) {
                hw_Led2_parpadeando();
                contador++;
            }
            else if (evTick5Hz && (contador == EROGACION_CAFE)){
                contador = 0;
                hw_Apagar_led2();
                hw_Encender_alarma();
                state = ALARMA;
            }
            break;

       case ALARMA:
            if (evTick1Hz && (contador < ESPERA_2_SEG)){
            contador++;
            }
            else if (evTick1Hz && (contador == ESPERA_2_SEG)) {
                hw_ApagarAlarma();
                state = ESPERANDO_FICHA;
            }
            break;
    }

    clearEvents();
}

void fsm_maquina_cafe_evTick1Hz(void)
{
    evTick1Hz = 1;
}

void fsm_maquina_cafe_evTick5Hz(void)
{
    evTick5Hz = 1;
}

void fsm_maquina_cafe_evDetector(void)
{
    evDetector = 1;
}

void fsm_maquina_cafe_evSeleccion1(void)
{
    evSeleccion1 = 1;
}

void fsm_maquina_cafe_evSeleccion2(void)
{
    evSeleccion2 = 1;
}

void fsm_maquina_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

/*==================[end of file]============================================*/
