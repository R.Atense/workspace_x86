/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;
uint8_t estado_led1 = 0;
uint8_t estado_led2 = 0;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_Led1_parpadeando(void)
{
	if (estado_led1){
		estado_led1 = 0;
		printf("Led 1 prendido \n\n");
	}
	else {
		estado_led1 = 1;
		printf("Led 1 apagado \n\n");
	}
}

void hw_Led1_encendido(void)
{
	printf("Led 1 encendido \nficha ingresada \n\n");
}

void hw_Led2_parpadeando(void)
{
	if (estado_led2){
		estado_led2 = 0;
		printf("Led 2 prendido \n\n");
	}
	else {
		estado_led2 = 1;
		printf("Led 2 apagado \n\n");
	}
}

void hw_Apagar_led2(void)
{
	printf("Led 2 apagado \n\n");
}

void hw_Encender_alarma(void)
{
	printf("Alarma activada \n\n");
}

void hw_ApagarAlarma(void)
{
	printf("Alarma desactivada \n\n");
}

/*==================[end of file]============================================*/
