/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_maquina.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont1_ms = 0;
    uint16_t cont2_ms = 0;

    hw_Init();

    fsm_maquina_init();

    while (input != EXIT) 
    {
        input = hw_LeerEntrada();

        // En un microcontrolador estos eventos se generarian aprovechando las
        // interrupciones asociadas a los GPIO
        if (input == SELECCION_1) 
            {
            fsm_maquina_cafe_evSeleccion1();
            }

        if (input == SELECCION_2) 
            {
            fsm_maquina_cafe_evSeleccion2();
            }
        if (input == FICHA)
            {
            fsm_maquina_cafe_evDetector();
            }

        // En un microcontrolador esto se implementaria en un handler de
        // interrupcion asociado a un timer
        cont1_ms++;
        cont2_ms++;
        if (cont1_ms == 1000) 
            {
            cont1_ms = 0;
            fsm_maquina_cafe_evTick1Hz();
            }

        if (cont2_ms == 200) 
            {
            cont2_ms = 0;
            fsm_maquina_cafe_evTick5Hz();
            }

        fsm_maquina_runCycle();

        // Esta funcion hace que el sistema no responda a ningun evento por
        // 1 ms. Funciones bloqueantes de este estilo no suelen ser aceptables
        // en sistemas baremetal.
        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
